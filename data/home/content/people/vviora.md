
---
type: team
title: Varvara Kaplenko
id: vviora
order: 51
language: en
image: images/people/vviora.png
---

**Project manager **

## About me
* Astrophysicist, who decided to get closer with programming 
* I’m in love with science and I want to integrate it in business 
* Project manager by day, esportsman by night  
* I believe that gaining soft skills is one of the most crucial things in life

## Education
* Bachelor’s degree in Fundamental Physics 
* Internship at Astro Space Center of Lebedev Physics Institute 
* Student at MIPT